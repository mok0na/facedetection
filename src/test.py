from PIL import Image
import numpy as np
import sys 
import matplotlib.pyplot as plt
import utils
import detection

model_path = "../data/Model/"
image_path = "../data/BioID/BioID_0001."

image = utils.read_pgm(image_path + "pgm")
image = utils.scale(image, 0.5)
# image = utils.im2array(Image.open("minImage.png").convert('L'))
# print(image.shape)
faceModel = utils.read_image(model_path + "faceModel.png")
print(faceModel.shape)
# im = Image.open("./testeyeModel.png").convert('L')
eyeModel = utils.read_image(model_path + "eyeModel.png")
Cl,Cr = utils.read_eye(image_path + "eye")

test = detection.Detection()

test.detect(image, faceModel, eyeModel)
# print("d_eye : ",test.evaluate(eyeModel.shape, Cl*0.2, Cr*0.2))
print("d_eye : ",test.evaluate(eyeModel.shape, Cl*0.5, Cr*0.5))

fig = plt.figure()
ax1 = fig.add_subplot(221)  # left side
ax2 = fig.add_subplot(222)  # right side
ax3 = fig.add_subplot(223)
ax4 = fig.add_subplot(224)
ax1.imshow(image,"gray")
ax2.imshow(utils.transform(faceModel, test.p1, image.shape), "gray")
ax3.imshow(test.refinedImage,"gray")
ax4.imshow(utils.transform(eyeModel, test.p2, test.refinedImage.shape), "gray")
plt.show()
