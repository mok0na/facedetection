import numpy as np

class HausdorffDistance():

    def H(self, I, M, lowerBounds, upperBounds):
        """
            Modified Hausdorff Distance
                I image
                M model
        """
        return max(self.h_mod(M,I), self.h_box(I ,M, lowerBounds, upperBounds))

    def h_mod(self, A, B):
        """
            forward Hausdorff Distance
        """
        sum = 0.0
        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                sum += np.min(np.linalg.norm(A[i,j] - B))
        sum /= A.shape[0] * A.shape[1] 
        return sum

    def h_box(self, A, B, lowerBounds, upperBounds):
        """
            box-reversed Hausdorff Distance
                lowerBounds box lower bounds
                upperBounds box upper bounds
        """
        sum = 0.0
        tot = 0
        for i in range(lowerBounds[0], min(upperBounds[0], A.shape[0])):
            for j in range(lowerBounds[1], min(upperBounds[1],A.shape[1])):
                sum += np.min(np.linalg.norm(A[i,j] - B))
                tot +=1
        sum /= tot
        return sum
