import hausdorff
import utils
import json
import numpy as np
from tqdm import tqdm

class Detection(object):
    def __init__(self, paramSpace1 = "paramSpace1.json", paramSpace2 = "paramSpace2.json", verbose = True):
        self.paramSpace1 = paramSpace1
        self.paramSpace2 = paramSpace2
        self.p1 = []
        self.p2 = [1.0,0.0,0,0]
        self.refinedImage = None
        self.verbose = verbose

    def detect(self, image, faceModel, eyeModel):
        # step 1 Coarse detection
        self.coarseDetection(image, faceModel)
        # step 2 refinement
        self.refinementDetection(image, faceModel, eyeModel)
        
    def coarseDetection(self, image, faceModel):
        # step 1 Coarse detection
        if (self.verbose):
            print("Coarse detection ...")
        # segmentation
        binImage = self.segmentation(image)
        # localization
        self.p1 = self.localization_1(binImage, faceModel, self.paramSpace1)
        print("p1 ", self.p1)

    def refinementDetection(self, image, faceModel, eyeModel):
        # step 2 refinement
        if (self.verbose):
            print("Refinement detection ...")
        # resampled AOI (Area of Interest)
        self.refinedImage = self.resample(image, self.p1, faceModel.shape)
        # segmentation
        binRefinedImage = self.segmentation(self.refinedImage, 9)
        # localization
        self.p2 = self.localization_2(binRefinedImage, eyeModel, self.paramSpace2)
        print("p2 ", self.p2)

    def resample(self, image, p, shape):
        resampled = utils.resample(image, p, shape)
        # i_min = int(resampled.shape[0] * 0.30)
        # i_max = int(resampled.shape[0] * 0.80)
        # return resampled[i_min:i_max]
        return resampled

    def segmentation(self, image, blocksize = 15):
        return utils.edgedetect(image, blocksize)

    def localization_1(self, image, model, paramSpaceFile):
        """
            detection problem : 
            find the transformation parameters p such the HD between the transformed model Tp(B) and image A is minimized
                paramSpace JSON filename [scale[min,max], angle[min,max], translation_x[step,min,max], translation_y[step,min,max]]
        """
        p = []
        paramSpace = json.load(open(paramSpaceFile))
        HD = hausdorff.HausdorffDistance()
        min_hd = 100000.0
        # compute parameters
        scales = np.linspace(paramSpace["scale"]["min"], paramSpace["scale"]["max"], paramSpace["scale"]["N"])
        angles =  np.linspace(paramSpace["angle"]["min"], paramSpace["angle"]["max"], paramSpace["angle"]["N"])

        # model fitting
        for scale in scales:
            Tp_B = np.zeros(image.shape, int)
            # scaling
            transformed = utils.scale(model, scale)
            for angle in angles:
                # rotation
                if (angle != 0.0):
                    transformed = utils.scale(transformed, angle)
                i_min = paramSpace["translation_i"]["min"]
                i_max = min(paramSpace["translation_i"]["max"], image.shape[0] - transformed.shape[0])
                j_min = paramSpace["translation_j"]["min"]
                j_max = min(paramSpace["translation_j"]["max"], image.shape[1] - transformed.shape[1])
                #  resizing
                Tp_B[:transformed.shape[0], :transformed.shape[1]] = transformed 
                if (self.verbose):
                    print("scale {} angle {}".format(scale, angle))
                    for ti in tqdm(range(i_min, i_max +1, paramSpace["translation_i"]["step"])):
                        for tj in range(j_min, j_max +1, paramSpace["translation_j"]["step"]):
                            # translation
                            corrected_ti = int(ti - np.sin(utils.deg2rad(angle))*model.shape[1])
                            Tp_B = utils.translate(Tp_B, [corrected_ti,tj])
                            # compute hausdorff distance
                            # hd = HD.H(image, model, box_lowerbounds, box_upperbounds)
                            hd = HD.h_mod(image, Tp_B)
                            if (min_hd > hd):
                                min_hd = hd
                                p = [scale, angle, ti, tj]
                else:
                    for ti in range(i_min, i_max +1, paramSpace["translation_i"]["step"]):
                        for tj in range(j_min, j_max +1, paramSpace["translation_j"]["step"]):
                            # translation
                            corrected_ti = int(ti - np.sin(utils.deg2rad(angle))*model.shape[1])
                            Tp_B = utils.translate(Tp_B, [corrected_ti,tj])
                            # compute hausdorff distance
                            # hd = HD.H(image, model, box_lowerbounds, box_upperbounds)
                            hd = HD.h_mod(image, Tp_B)
                            if (min_hd > hd):
                                min_hd = hd
                                p = [scale, angle, ti, tj]
        return p

    def localization_2(self, image, model, paramSpaceFile):
        """
            detection problem : 
            find the transformation parameters p such the HD between the transformed model Tp(B) and image A is minimized
                paramSpace JSON filename [scale[min,max], angle[min,max], translation_x[step,min,max], translation_y[step,min,max]]
        """
        p = []
        paramSpace = json.load(open(paramSpaceFile))
        HD = hausdorff.HausdorffDistance()
        min_hd = 100000.0
        # compute parameters
        scales = np.linspace(paramSpace["scale"]["min"], paramSpace["scale"]["max"], paramSpace["scale"]["N"])
        angles =  np.linspace(paramSpace["angle"]["min"], paramSpace["angle"]["max"], paramSpace["angle"]["N"])

        # model fitting
        for scale in scales:
            Tp_B = np.zeros(image.shape, int)
            # scaling
            transformed = utils.scale(model, scale)
            for angle in angles:
                # rotation
                if (angle != 0.0):
                    transformed = utils.scale(transformed, angle)
                i_min = paramSpace["translation_i"]["min"]
                i_max = min(paramSpace["translation_i"]["max"], image.shape[0] - transformed.shape[0])
                j_min = paramSpace["translation_j"]["min"]
                j_max = min(paramSpace["translation_j"]["max"], image.shape[1] - transformed.shape[1])
                #  resizing
                Tp_B[:transformed.shape[0], :transformed.shape[1]] = transformed 
                print("scale {} angle {}".format(scale, angle))
                for ti in tqdm(range(i_min, i_max +1, paramSpace["translation_i"]["step"])):
                    for tj in range(j_min, j_max +1, paramSpace["translation_j"]["step"]):
                        # translation
                        corrected_ti = int(ti - np.sin(utils.deg2rad(angle))*model.shape[1])
                        Tp_B = utils.translate(Tp_B, [corrected_ti,tj])

                        # compute hausdorff distance
                        box_lowerbounds = np.array([corrected_ti, tj])
                        box_upperbounds = box_lowerbounds + np.array(transformed.shape)
                        assert len(box_upperbounds) == 2 
                        # hd = HD.H(image, model, box_lowerbounds, box_upperbounds)
                        hd = HD.H(image, Tp_B, box_lowerbounds, box_upperbounds)
                        if (min_hd > hd):
                            min_hd = hd
                            p = [scale, angle, ti, tj]
        return p
    
    def evaluate(self, shape, Cl, Cr):
        # TODO compute with rotation or MLP
        model_dl = np.array([shape[0] * 0.38, shape[1] * 0.80])
        model_dr = np.array([shape[0] * 0.38, shape[1] * 0.20])
        # model_dl = np.array([7, 22])
        # model_dr = np.array([7, 5])
        tmp = np.array(self.p1[2:4]) + np.array(self.p2[2:4])
        dl = np.linalg.norm(tmp + self.p2[0] * model_dl - Cl)
        dr = np.linalg.norm(tmp + self.p2[0] * model_dr - Cr)
        if (self.verbose):
            print("p Cr Cl", tmp + self.p2[0] * model_dr, tmp + self.p2[0] * model_dl)
            print("t Cr Cl", Cr, Cl)
            print(dl, dr)
            print(np.linalg.norm(Cl - Cr))
        d_eye = max(dl,dr)/ np.linalg.norm(Cl - Cr)
        return d_eye