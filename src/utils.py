from PIL import Image
import numpy as np
import sys 
import matplotlib.pyplot as plt
from scipy import ndimage
from skimage.filters import threshold_local

def read_pgm(pgm_file):
    """ 
        read pmg file
    """ 
    f = open(pgm_file,"rb")
    assert f.readline()[:2].decode("utf-8") == "P5"
    return im2array(Image.open(pgm_file))

def im2array(img, type = np.float):
    return np.array(img, dtype=type)

def array2im(arr, mode = "L"):
    return Image.fromarray(arr, mode)

def read_eye(eye_file):
    """
        read eye position from .eye file
    """
    f = open(eye_file,"r")
    # skip the first line
    f.readline()
    line = f.readline().split("\t")
    Cl = np.array([float(line[1]), float(line[0])])
    Cr = np.array([float(line[3]), float(line[2])])
    return Cl, Cr

def read_image(image_path):
    im = Image.open(image_path).convert('L')
    return im2array(im, int)

def loadData(image_path = "../data/BioID/BioID_", n_image = 1362):
    data = []
    for i in range(n_image):
        image = dict()
        image["path"] = image_path + '{:04d}.pgm'.format(i)
        image["Cl"],image["Cr"] = read_eye(image_path + '{:04d}.eye'.format(i))
        # TODO rescale
        image["Cl"],image["Cr"] = 0.3 * image["Cl"], 0.3 * image["Cr"]
        data.append(image)
    return data

def binarize(image, blocksize):
    local_thresh = threshold_local(image, blocksize, offset=10)
    return np.array(image < local_thresh, int)

def edgedetect(arr, blocksize = 15):
    """
        edge detection with sobel operator
        resulting binary edge point
    """
    dx = ndimage.sobel(arr, 1, mode='constant')
    dy = ndimage.sobel(arr, 0, mode='constant')
    mag = np.hypot(dx, dy)
    mag *= 255.0 / np.max(mag)
    return binarize(mag, blocksize)

def scale(image, scale):
    return ndimage.zoom(image, scale)

def rotate(image, angle):
    return ndimage.rotate(image, angle)

def translate(image, translation):
    return ndimage.shift(image, translation)

def transform(image, p, shape):
    """
        Image transformation
            p parameter [scale, angle, translation_x, translation_y] 
                angle in degree
                translation_x w.r.t matrix = ty
            shape shape of the final image
    """
    transformed = image
    if p[0] != 1.0:
        transformed = scale(transformed, p[0])
    if p[1] != 0.0:
        transformed = rotate(transformed, p[1])
    resized = np.zeros(shape, int)
    resized[:transformed.shape[0], :transformed.shape[1]] = transformed 
    # substract rotation translation
    p[2] = int(p[2] - np.sin(deg2rad(p[1]))*image.shape[1]) 
    resized = translate(resized, p[2:])
    return resized

def deg2rad(degree):
    return degree*np.pi/180.0

def resample(image, p, shape):
    """
        Image resample
            p parameter [scale, angle, translation_i, translation_j]
                angle in degree
            shape shape of the model
    """
    rad_angle = deg2rad(p[1])
    i0= max(int(p[2] - np.sin(rad_angle)*shape[1] * p[0]),0)
    i1= int(p[2] + np.cos(rad_angle)*shape[0] * p[0] )
    j0= p[3] 
    j1= int(p[3] + (np.sin(rad_angle)*shape[0] + np.cos(rad_angle)*shape[1])*p[0])
    resampled = image[i0:i1,j0:j1]
    if p[1] != 0.0:
        resampled = rotate(resampled, -p[1])
    return resampled

# im = read_pgm("../data/BioID/BioID_0001.pgm")
# arr = im2array(im)

# test edgedetect 
# result = edgedetect(arr)
# plt.imshow(result)
# plt.show()

# test transform
# fig = plt.figure()
# ax1 = fig.add_subplot(121)  # left side
# ax2 = fig.add_subplot(122)  # right side
# image = edgedetect(arr, 15)
# m = Image.open("../data/Model/faceModel.png")
# model = im2array(m, np.int)
# p = [4.5,0.0,0,70]
# transformed_model = transform(model, p, arr.shape)
# print(model.shape)
# print(transformed_model)

# lbl = ndimage.label(result[60:140,131:190])[0]
# print(ndimage.measurements.center_of_mass(result[60:140,131:190], lbl, [1,2]))

# resampled = resample(arr,p, model.shape)
# binRefined = edgedetect(resampled,9)
# print(binRefined.shape)
# print(binRefined)
# ax2.imshow(transformed_model, "gray")
# ax1.imshow(binRefined, "gray")

#ax2.imshow(result, "gray")
# plt.imshow(arr, "gray")
# plt.show()

# plt.imsave("minImage.png",scale(arr, 0.2), cmap="gray")
# plt.imsave("testfaceModel.png",scale(image[50:200,120:260], 0.2), cmap="gray")
# plt.imsave("testeyeModel.png",scale(image[60:150,120:260], 0.2), cmap="gray")

loadData()