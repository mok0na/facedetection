import numpy as np
import random
import detection
import utils

class GA(object):
    def __init__(self, n_population=50, n_offspring=25, pr_crossover=0.7, 
                 pr_mutation=0.1, max_generations=5, var=5,
                 n_individual = (45,24),
                 n_model = (45,48),
                 method = "blank"):
        """
            Algorithm parameters
                n_population Population size
                n_offspring Number of offspring individuals
                pr_crossover Crossover probability
                pr_mutation Mutation probability
                max_generations
                var Noise level for mutation
                n_individual size of individual
                n_model size of model
                method method of the initialization
        """
        self.n_population = n_population
        self.pr_crossover = pr_crossover
        self.pr_mutation = pr_mutation
        self.var = var
        self.n_offspring = n_offspring
        self.max_generations = max_generations
        self.n_individual = n_individual
        self.n_model = n_model

        self.lpopulation = []
        self.offsprings = []
        self.lmatingPool = []

        self.detection = detection.Detection(verbose = False)

    def optimization(self, init_method, image_path, n_image, n_subset, rank):
        dataset = utils.loadData(image_path, n_image)
        # Initialize population
        self.initPopulation(init_method)
        for ind in self.lpopulation:
            utils.plt.imshow(ind.getModel(self.n_model), "gray")
            utils.plt.show()
        # while not converged
        converged = False
        subset = []
        # model = utils.read_image(model_path)
        while not converged:
            print("===optimisation===")
            # evaluate population on complete set
            self.computeFitness(dataset)
            # find the best model
            self.bestModel = self.selectBestModel()
            # build new evaluation set with best model
            subset = self.buildEvaluationSet(self.bestModel, dataset, n_subset, rank)
            # run GA for five generation
            self.run(subset)
            utils.plt.imshow(self.bestModel.getModel(self.n_model), "gray")
            utils.plt.show()
            # TODO converged?

    def buildEvaluationSet(self, bestModel, dataset, n_subset, rank = 200):
        """
            select 40 out of 200 top-ranking images and 40 out of 200 lowest ranking image
        """
        # compute d_eye with the bestModel
        self.evaluateModel(bestModel, dataset, True)
        ld_eye = np.array([image["d_eye"] for image in dataset])
        indexes = np.argsort(ld_eye)
        # get 40 out of 200 top-ranking image
        top = np.random.choice(indexes[:rank], int(n_subset/2))
        # get 40 out of 200 lowest ranking image
        lowest = np.random.choice(indexes[-rank:], int(n_subset/2))
        subset = []
        for i in top:
            subset.append(dataset[i])
        for i in lowest:
            subset.append(dataset[i])
        return subset

    def run(self, evaluationSet):
        # #step 1 generate an initial population and set counter to 0
        # self.initPopulation()
        m = 0
        while m < self.max_generations:
            print("====generation {}====".format(m))
            #step 3 environmental selection
            print("env selection")
            self.environmentalSelection()
            #step 4 mating pool and crossover
            print("parent selection")
            self.parentSelection()
            self.crossover()
            #step 5 mutation
            print("mutation")
            self.mutation()
            #step 2 compute fitness
            self.computeFitness(evaluationSet)
            m += 1

    def initPopulation(self, method = "blank"):
        for i in range(self.n_population):
            individual = Individual(self.n_individual,method)
            self.lpopulation.append(individual)

    # TODO handle faceModel and eyeModel
    def computeFitness(self, evaluationSet):
        """
            compute fitness with coarseDetection
        """
        for ind in self.lpopulation:
            self.evaluateModel(ind, evaluationSet)

    def evaluateModel(self, model, evaluationSet, store_deye = False):
        rate = 0
        for image in evaluationSet:
            # TODO delete rescale
            im = utils.read_pgm(image["path"])
            im = utils.scale(im, 0.3)
            self.detection.coarseDetection(im, model.getModel(self.n_model))
            d_eye = self.detection.evaluate(im.shape, image["Cl"], image["Cr"])
            if (store_deye) :
                image["d_eye"] = d_eye
            if ( d_eye <= 0.12 ):
                rate +=1
        model.fitness = rate / len(evaluationSet)

    def selectBestModel(self):
        lfitness = [ind.fitness for ind in self.lpopulation]
        i_best = lfitness.index(max(lfitness))
        return self.lpopulation[i_best]

    def environmentalSelection(self):
        N = len(self.lpopulation)
        print(N,self.n_population)
        while N > self.n_population:
            #choose x* with the smallest fitness value
            lfitness = [ind.fitness for ind in self.lpopulation]
            i_min = lfitness.index(min(lfitness))
            #remove it 
            self.lpopulation.pop(i_min)
            N = N -1


    def parentSelection(self):
        self.lmatingPool = []
        n = 0
        while n < 2*self.n_offspring:
            S = np.sum([ind.fitness for ind in self.lpopulation])
            pick = random.uniform(0, S)
            current = 0
            for i in np.random.shuffle(range(len(self.lpopulation))):
                current += self.lpopulation[i].fitness
                if current >= pick:
                    self.lmatingPool.append(self.lpopulation[i])
                    break
            n += 1

    def crossover(self):
        self.offsprings = []
        child = Individual(self.n_individual)
        for i in range(0,self.n_offspring,2):
            if (random.random() >= 0.5):
                parent1 = self.lmatingPool[i]
                parent2 = self.lmatingPool[i+1]
            else:
                parent1 = self.lmatingPool[i+1]
                parent2 = self.lmatingPool[i]
            n = int(random.uniform(0,self.n_individual[0]))
            m = int(random.uniform(0,self.n_individual[1]))
            child.values[0:n,0:m] = parent1.values[0:n,0:m] 
            child.values[n:,m:] = parent1.values[n:,m:]
            child.values[0:n,m:] = parent2.values[0:n,m:]
            child.values[n:,0:m] = parent2.values[n:,0:m]
            self.offsprings.append(child)

    def mutation(self, eta_m=1):
        for i in range(len(self.lpopulation)):
            individual = self.lpopulation[i]
            R = 1 * (np.random.random(self.n_individual) < self.pr_mutation)
            individual.values = np.abs(R-individual.values)

class Individual:
    # TODO average and hand-drawn model
    def __init__(self, n_individual, method = "blank"):
        self.fitness = 0
        if (method == "blank"):
            self.values = 1 * (np.random.random(n_individual) < 0.05)
        self.size = 0
        self.objectiveFunction = 0
        self.n_individual = n_individual

    def getModel(self, n_model):
        model = np.zeros((n_model), int)
        model[:self.n_individual[0],:self.n_individual[1]] = self.values
        model[:self.n_individual[0],n_model[1]-1:self.n_individual[1]-1:-1] = self.values
        return model
        
                
def main():
    ga = GA(n_population=2, n_offspring=1)
    ga.optimization("blank", "../data/BioID/BioID_", n_image = 3,n_subset = 2, rank = 1)

if __name__ == '__main__':
    main()
